+++
date = "2017-06-06T18:03:00+02:00"
title = "Je veux publier ma these"
image = "img/Preview/these.png"
draft = false

+++


**TODO cc-by:**

 * définir droit d'auteur pour montrer importance des CC dans le partage.
 * utiliser `ça` pour tager les droits/possibilités: faire ressortir chaque type de cc.


Les principales licences permettant de publier une thèse sous licence ouverte sont les **Creative Commons**.

Nous allons vous proposer ici un bref **résumé** de ce groupe de licences, vous aurez le choix entre du **texte** ou une **vidéo**. Puis vous pourrez ensuite choisir la version Creative Commons qui vous conviendra le mieux. Vous n'aurez plus qu'à copier-coller le logo correspondant dans votre thèse ou simplement l'indiquer par écrit.<!--more-->

## La vidéo ##

{{< vimeo 116163416 >}}

## Le résumé ##

Les licences Creative Commons constituent un ensemble de licences régissant les conditions de réutilisation et de distribution d'œuvres (notamment d'œuvres multimédias diffusées sur Internet).

Élaborées par l'organisation [Creative Commons](https://fr.wikipedia.org/wiki/Creative_Commons), elles ont été publiées le 16 décembre 2002.

>Source: Wikipedia - Licence Creative Commons - cc-by-sa 3.0

Mais pourquoi préciser sur ma thèse que je souhaite permettre le partage de mon travail?


`Pause ici`

Creative Commons propose des contrats-type ou licences pour la mise à disposition d’œuvres en ligne. Inspirés par les licences libres, les mouvements open source et open access, ces licences facilitent l’utilisation d’œuvres (textes, photos, musique, sites web, etc).

Ces licences s’adressent aux auteurs souhaitant :

 * partager et faciliter l’utilisation de leur création par d’autres
 * autoriser gratuitement la reproduction et la diffusion (sous certaines conditions)
 * accorder plus de droits aux utilisateurs en complétant le droit d’auteur qui s’applique par défaut
 * faire évoluer une oeuvre et enrichir le patrimoine commun (les biens communs ou Commons)
 * économiser les coûts de transaction
 * légaliser le peer to peer de leurs œuvres.

Nous vous proposons ici une vidéo de présentation de ce type de licence.



## RÉSUMÉ ##


Les licences Creative Commons sont fondées sur le droit d’auteur. Alors que le régime du droit d’auteur classique vous incite à garder l’exclusivité sur la totalité de vos droits (« tous droits réservés »), ces licences vous encouragent à n’en conserver qu’une partie (« certains droits réservés »). Creative Commons travaille avec des experts en droit d’auteur dans le monde entier pour que ces licences soient valides quelle que soit la juridiction. Ces licences permettent au public d’utiliser vos œuvres, sous certaines conditions, selon vos préférences. Les licences sont modulables et existent sous 3 formes :


un résumé explicatif destiné  aux utilisateurs non-juristes, il décrit de manière simple les actes que le public a le droit d’effectuer sur l’œuvre;
un contrat destiné aux juristes;
une version en code informatique, permettant d’établir un lien vers le résumé et d’associer des métadonnées à l’œuvre.
Conditions communes à toutes les licences Creative Commons

Offrir une autorisation non exclusive de reproduire, distribuer et communiquer l’œuvre au public à titre gratuit, y compris dans des œuvres dites collectives.
Faire apparaître clairement au public les conditions de la licence de mise à disposition de cette création, à chaque utilisation ou diffusion.
Chacune des conditions optionnelles peut être levée après l’autorisation du titulaire des droits.
Les exceptions au droit d’auteur ne sont en aucun cas affectées.
Il est interdit d’utiliser des mesures techniques contradictoires avec les termes des licences.
Le partage de fichiers (peer to peer) n’est pas considéré comme une utilisation commerciale.
Comment faire pour placer vos œuvres sous l’une des licences Creative Commons ?

La procédure se fait en ligne, il n’y a aucun document à signer. Si vous êtes auteur, ou avec l’accord des titulaires de droits, vous pouvez simplement choisir votre licence parmi les 6 combinaisons d’options existantes en répondant à quelques questions sur notre interface (www.creativecommons.fr).

Lorsque vous sélectionnez une licence Creative Commons, vous obtiendrez un morceau de code html/rdf qui peut facilement être inséré sur une page web. Ce code reproduira sur le site le logo Creative Commons avec un lien vers la version résumée de la licence sélectionnée. Vous pouvez insérer à côté de ce logo une phrase pour expliquer que les œuvres placées sur votre site sont sous l’une des licences Creative Commons.

Toute copie ou communication de l’œuvre au public doit être accompagnée de la licence selon laquelle elle a été mise à la disposition du public, ou d’un lien vers  le texte de cette licence. Certains formats peuvent être marqués directement (http://creativecommons.org/technology/usingmarkup).

Creative Commons propose gratuitement six licences qui permettent aux titulaires de droits d’auteur de mettre leurs oeuvres à disposition du public à des conditions prédéfinies. Les licences Creative Commons viennent en complément du droit applicable, elles ne se substituent pas au droit d’auteur.
Simples à utiliser et intégrées dans les standards du web, ces autorisations non exclusives permettent aux titulaires de droits d’autoriser le public à effectuer certaines utilisations, tout en ayant la possibilité de réserver les exploitations commerciales, les oeuvres dérivées et les conditions de redistribution.

LES OPTIONS
Les auteurs ou titulaires des droits d’auteur peuvent choisir un ensemble de conditions qu’ils souhaitent appliquer à leurs oeuvres:


ATTRIBUTION :  Toutes les licences Creative Commons obligent ceux qui utilisent vos oeuvres à vous créditer de la manière dont vous le demandez, sans pour autant suggérer que vous approuvez leur utilisation ou leur donner votre aval ou votre soutien.


PAS D’UTILISATION COMMERCIALE :  Vous autorisez les autres à reproduire, à diffuser et (à moins que vous choisissiez ‘Pas de Modification’) à modifier votre œuvre, pour toute utilisation autre que commerciale, à moins qu’ils obtiennent votre autorisation au préalable.


PARTAGE DANS LES MEMES CONDITIONS: Vous autorisez les autres à reproduire, diffuser et modifier votre œuvre, à condition qu’ils publient toute adaptation de votre œuvre sous les mêmes conditions que votre oeuvre. Toute personne qui souhaiterait publier une adaptation sous d’autres conditions doit obtenir votre autorisation préalable.


PAS DE MODIFICATION: Vous autorisez la reproduction et la diffusion uniquement de l’original de votre oeuvre. Si quelqu’un veut la modifier, il doit obtenir votre autorisation préalable.

LES LICENCES
Ces quatre options peuvent être arrangées pour créer six licences différentes, les six licences Creative Commons :
ATTRIBUTION

ATTRIBUTION / PAS DE MODIFICATION


ATTRIBUTION / PAS D’UTILISATION COMMERCIALE / PAS DE MODIFICATION



ATTRIBUTION / PAS D’UTILISATION COMMERCIALE


ATTRIBUTION / PAS D’UTILISATION COMMERCIALE / PARTAGE DANS LES MÊMES CONDITIONS



ATTRIBUTION / PARTAGE DANS LES MÊMES CONDITIONS


1. Attribution (BY): Le titulaire des droits autorise toute exploitation de l’œuvre, y compris à des fins commerciales, ainsi que la création d’œuvres dérivées, dont la distribution est également autorisé sans restriction, à condition de l’attribuer à son l’auteur en citant son nom. Cette licence est recommandée pour la diffusion et l’utilisation maximale des œuvres.

2. Attribution + Pas de Modification (BY ND) : Le titulaire des droits autorise toute utilisation de l’œuvre originale (y compris à des fins commerciales), mais n’autorise pas la création d’œuvres dérivées.

3. Attribution + Pas d’Utilisation Commerciale + Pas de Modification (BY NC ND) : Le titulaire des droits autorise l’utilisation de l’œuvre originale à des fins non commerciales, mais n’autorise pas la création d’œuvres dérivés.

4. Attribution + Pas d’Utilisation Commerciale (BY NC) : le titulaire des droits autorise l’exploitation de l’œuvre, ainsi que la création d’œuvres dérivées, à condition qu’il ne s’agisse pas d’une utilisation commerciale (les utilisations commerciales restant soumises à son autorisation).

5. Attribution + Pas d’Utilisation Commerciale + Partage dans les mêmes conditions (BY NC SA): Le titulaire des droits autorise l’exploitation de l’œuvre originale à des fins non commerciales, ainsi que la création d’œuvres dérivées, à condition qu’elles soient distribuées sous une licence identique à celle qui régit l’œuvre originale.

6. Attribution + Partage dans les mêmes conditions (BY SA) : Le titulaire des droits autorise toute utilisation de l’œuvre originale (y compris à des fins commerciales) ainsi que la création d’œuvres dérivées, à condition qu’elles soient distribuées sous une licence identique à celle qui régit l’œuvre originale. Cette licence est souvent comparée aux licences « copyleft » des logiciels libres. C’est la licence utilisée par Wikipedia.

Il ne vous reste plus qu'à choisir la version des creative commons qui vous convient le mieux sur le site de [creativecommons.org](https://creativecommons.org/choose/).
